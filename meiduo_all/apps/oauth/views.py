import json
import re

from django.contrib.auth import login
from django.http import JsonResponse

from apps.oauth.models import OAuthQQUser
from apps.users.models import User
from meiduo_all import settings
# Create your views here.
# 根据oauth2.0来获取code和token
# 生成用户链接 : 获取code=>获取token=>获取openid=>保存openid
from django.views import View
from QQLoginTool.QQtool import OAuthQQ

from utils.Views import LoginRequiredJsonMixin

"""
生成用户连接
前端:当用户点击qq登录图标的时候，前端应该发送一个axios(ajax)请求
后端
    请求
    业务逻辑 调用qqlogintool生成跳转连接
    响应 返回跳转路径
    路由 GET
    步骤:
        1.生成qqlogintool实例对象
        2.调用对象生成跳转连接
        3.返回响应"""


class QQLoginURLView(View):
    def get(self,request):
        # 1.生成qqlogintool实例对象
        """
        client_id = app_id
        client_secret = appsecret
        redirect_uri  用户同意登录之后的跳转的页面
        state : 随便写
        :param request:
        :return:
        """
        qq = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                     client_secret=settings.QQ_CLIENT_SECRET,
                     redirect_uri=settings.QQ_REDIRECT_URL,
                     state='xxxx')
        # 2.调用对象生成跳转连接
        qq_login_url = qq.get_qq_url()
        # 返回响应
        return JsonResponse({'code':0,'errmsg':'ok','login_url':qq_login_url})


class OauthQQView(View):
    """
    需求: 绑定用户信息
    前端:
        当用户输入 手机号，密码，短信验证码，之后就发送axios请求，请求需要mobile,password,sms_code,access_token(openid)
    后端:
        请求: 接收，获取参数
        业务逻辑: 绑定，完成状态保持
        响应: 返回code=0跳转首页
        路由: POST oauth_callback/
        步骤:
            1.接收请求
            2.获取请求参数
            3.根据手机号进行用户信息的查询
            4.查询到用户手机号已经注册了，判断密码是否正确，就可以保持绑定用户和openid信息
            5.没有注册手机号，就创建一个user信息再绑定
            6.进行状态保持
            7.返回响应
    """
    def post(self,request):
        data = json.loads(request.body.decode())
        # 获取参数
        mobile = data.get('mobile')
        password = data.get('password')
        sms_code = data.get('sms_code')
        open_id = data.get('access_token')
        if not all([mobile,password,sms_code,open_id]):
            return JsonResponse({'code': 400, 'errmsg': '参数不全'})
        if not re.match('1[3-9]\d{9}',mobile):
            return JsonResponse({'code': 400, 'errmsg': '请输入正确的手机号码'})
        if not re.match(r'[0-9A-Za-z]{8,20}',password):
            return JsonResponse({'code': 400, 'errmsg': '请输入8-20位密码'})

        # # 提取服务器的短信验证码
        # redis_cli = get_redis_connection('code')
        # sms_code_server = redis_cli.get(mobile)
        # # 判断短信验证码是否过期
        # if not sms_code_server:
        #     return JsonResponse({'code': 400, 'errmsg': '短信验证码已失效'})
        # # 对比用户输入的和服务器的短信验证码是否一致
        # if sms_code_server.decode() != sms_code:
        #     return JsonResponse({'code': 400, 'errmsg': '短信验证码有误'})
        # 对access_token 解密
        from apps.oauth.utils import check_access_token
        openid = check_access_token(open_id)
        if not openid:
            return JsonResponse({"code":400,"errmsg":"参数缺失"})
        try:
            user = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            # 手机号不存在
            user = User.objects.create_user(username=mobile,password=password,mobile=mobile)
        else:
            # 如果手机号存在
            if not user.check_password(password):
                return JsonResponse({'code':400,'errmsg':'账号或密码错误'})
        OAuthQQUser.objects.create(user=user,openid=openid)
        # 状态保持
        login(request,user)
        response = JsonResponse({'code': 0, 'errmsg': 'ok'})
        response.set_cookie('username', user.username)
        return response

    """
    需求: 获取code 通过code换取token，再通过token换openid
    前端:
        应该获取 用户同意登录的code，把这个code发送给后端
    后端:
        请求: 获取code
        业务逻辑: 通过code换token，token换openid,判断openid是否绑定
        路由: GET oauth_callback/?code=xxxx
        步骤:
        1.获取code
        2.通过code换取token
        3.再通过token换openid
        4.判断openid是否绑定账号
        5.没有绑定就绑定
        6.如果绑定过，则直接登录
    """
    def get(self,request):
        # 1.获取code
        code = request.GET.get('code')
        if code is None:
            return JsonResponse({'code':400,'errmsg':'参数不全'})
        # 2.通过code换取token
        qq = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                     client_secret=settings.QQ_CLIENT_SECRET,
                     redirect_uri=settings.QQ_REDIRECT_URL,
                     state='xxxx')
        token = qq.get_access_token(code)
        # 3.再通过token换openid
        openid = qq.get_open_id(token)
        # 4.判断openid是否绑定账号
        try:

            qquser = OAuthQQUser.objects.get(openid = openid)
        except OAuthQQUser.DoesNotExist:
            # 不存在
            # 5. 没有绑定就绑定
            from apps.oauth.utils import generic_openid
            access_token = generic_openid(openid)
            response = JsonResponse({'code':400,'access_token':access_token})
            return response
        else:
            # 存在
            # 6.如果绑定过，则直接登录
            # 6.1设置session
            login(request,qquser.user)
            # 6.2设置cookie
            response = JsonResponse({'code':0,'errmsg':'ok'})
            response.set_cookie('username',qquser.user.username)

            return response





