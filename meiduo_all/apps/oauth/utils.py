#####################itsdangerous的基本使用##################
# 数据加密
# 这个类不仅可以对数据加密，还可以设置对数据加密时效
from meiduo_all import settings
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
# 加密
def generic_openid(openid):
    # 创建类实例对象
    # secret_key 密钥
    # expires_in = None 数据过期时间
    s = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    access_token = s.dumps({'openid': openid})
    # 讲bytes类型转str
    return access_token.decode()
# 解密
def check_access_token(token):
    s = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    try:
        result = s.loads(token)
    except Exception:
        return None
    else:
        return result.get('openid')

    