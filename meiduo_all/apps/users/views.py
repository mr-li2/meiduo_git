import json
import re

from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views import View
from django_redis import get_redis_connection

from apps.users.models import User
from apps.users.utils import generic_email_verify_token
from utils.Views import LoginRequiredJsonMixin

"""
判断用户名是否重复
前端:当用户输入用户名之后，失去焦点，发送一个ajax请求
后端:
    请求 : 接收用户名
    业务请求: 根据用户名查询数据库，如果查询结果等于0，说明没有注册
    响应: json数据
    {code:0,count:0/1,errmsg:ok}
    路由  GET usernames/<username>/count/
    步骤: 
    1.接收用户名
    2.根据用户名查询数据库
    3.返回响应
"""

class MobileCountView(View):
    def get(self,request,mobile):
        count = User.objects.filter(mobile=mobile).count()
        return JsonResponse({'code':0,'count':count,'errmsg':'ok'})

class UsernameCountView(View):
    def get(self,request,username):
        if re.match('[0-9]+',username):
            return JsonResponse({'code': 400, 'errmsg': '不能全是数字'})
        # 查询用户名的记录条数
        count = User.objects.filter(username=username).count()
        return JsonResponse({'code':0,'count':count,'errmsg':'ok'})
"""
前端:axiso发送注册信息
后端:
    请求:接收请求(json).获取数据
    业务逻辑: 验证数据，数据入库
    响应:json{'code':0,'errmsg':ok}
    路由：POST register/
    步骤:
    1.接收请求(post --json)
    2.获取数据
    3.验证数据
        3.1用户名，密码，确认密码，手机号，是否同意协议，都要有
        3.2用户名满足规则，用户名不能重复
        3.3密码满足规则
        3.4确认密码无误
        3.5手机号满足规则 手机号不能重复
        3.6需要同意协议
    4.数据入库
    5.返回响应
"""
class RegisterView(View):
    def post(self,request):
        # 接收json数据
        body_bytes = request.body
        body_str = body_bytes.decode()
        body_dict = json.loads(body_str)
        # 获取数据
        username = body_dict.get('username')
        password = body_dict.get('password')
        password2 = body_dict.get('password2')
        mobile = body_dict.get('mobile')
        allow = body_dict.get('allow')
        sms_code_client = body_dict.get('sms_code')

        # 验证数据
        # 只要有一个空就为False
        if not all([username,password,password2,mobile,sms_code_client,allow]):
            return JsonResponse({'code':400,'errmsg':'参数不全'})
        if (not re.match('[a-zA-Z0-9_-]{5,20}',username)) or re.match('[0-9]+',username):
            return JsonResponse({'code':400,'errmsg':'用户名不满足规则'})
        # 提取服务器的短信验证码
        redis_cli = get_redis_connection('code')
        sms_code_server = redis_cli.get(mobile)
        # 判断短信验证码是否过期
        if not sms_code_server:
            return JsonResponse({'code':400,'errmsg':'短信验证码已失效'})
        # 对比用户输入的和服务器的短信验证码是否一致
        if sms_code_server.decode() != sms_code_client:
            return JsonResponse({'code':400,'errmsg':'短信验证码有误'})
        # 数据入库
        # 方式1
        # user = User(username=username,password=password,mobile=mobile)
        # user.save()
        # 方式2
        # User.objects.create(username=username,password=password,mobile=mobile)
        # 以上两种方法password没有加密保存进数据库
        ##########
        # 这种方式password就加密了
        user = User.objects.create_user(username=username,password=password,mobile=mobile)
        # 系统(Django)为我们提供了 状态保持的方法
        from django.contrib.auth import login
        login(request,user)
        return JsonResponse({'code':0,'errmsg':'ok'})

"""
注册即登录
注册在手动登录
状态保持两种方式:
    在客户端存储信息使用cookie
    在服务器存储信息用session
"""
"""
前端:
    当用户把用户名和密码输入完成之后，会点击登录，发送axios请求
后端:
    请求: 接收数据 验证数据
    业务逻辑:验证用户名和密码，session
    响应:返回json 0成功 400失败
步骤:
    1.接收数据
    2.验证数据
    3.验证用户名和密码是否正确
    4.session
    5.判断是否记住登录
    6.返回响应
"""


class LoginView(View):
    def post(self,request):
        data = json.loads(request.body.decode())
        username = data.get('username')
        password = data.get('password')
        remembered = data.get('remembered')
        # 验证数据
        if not all([username,password]):
            return JsonResponse({'code':400,'errmsg':'参数不全'})
        # 判断用户是否以手机号登录
        if re.match('1[3-9]\d{9}',username):
            User.USERNAME_FIELD = 'mobile'
        else:
            User.USERNAME_FIELD = 'username'
        # 验证用户名和密码
        # 我们可以通过模型根据用户名哎查询
        # User.objects.get(username=username)
        # 方式2
        from django.contrib.auth import authenticate
        # authenticate 传递用户名和密码，正确就返回user信息不正确就返回None
        user = authenticate(username=username,password=password)
        if user is None:
            return JsonResponse({'code':400,'errmsg':'账号或密码错误'})
        # session
        from django.contrib.auth import login
        login(request,user)
        # 判断是否记住登录
        if remembered:
            # 记住登录
            # None默认2周
            request.session.set_expiry(None)
        else:
            # 不记住登录,浏览器关闭session过期
            request.session.set_expiry(0)

        response = JsonResponse({'code':0,'errmsg':'ok'})
        response.set_cookie('username',username)
        return response
"""
前端：
用户点击退出，前端发送一个delete请求
后端:
    请求
    业务逻辑 退出
    响应 json数据
"""
from django.contrib.auth import logout
class LogoutView(View):
    def delete(self,request):
        # 1.删除session
        logout(request)
        response = JsonResponse({'code':0,'errmsg':'ok'})
        # 2.删除cookie信息，因为前端是根据cookie来判断用户是否登录
        response.delete_cookie('username')
        return response
# 用户中心，也必须是登录用户
class CenterView(LoginRequiredJsonMixin,View):
    def get(self,request):
        # request.user 就是用户的登录信息
        info_data = {
            'username':request.user.username,
            'email':request.user.email,
            'mobile':request.user.mobile,
            'email_active':request.user.email_active,
        }
        return JsonResponse({'code':0,'errmsg':'ok','info_data':info_data})

"""
需求: 用户输入邮箱之后，点击保存。这个时候会发送axios请求
后端:
    请求 接收请求，获取数据
    业务逻辑 保存地址 发送一封邮件
    响应 json code=0
    步骤:
        接收请求
        获取数据
        保存邮箱地址
        发送一封激活邮件
        返回响应
"""
class EmailView(LoginRequiredJsonMixin,View):
    def put(self,request):
        # 接收请求
        data = json.loads(request.body.decode())
        # 获取数据
        email = data.get('email')
        # 验证数据
        # 验证正则
        # 保存邮箱地址
        user = request.user
        user.email = email
        user.save()
        # 发送激活邮件
        from django.core.mail import send_mail
        """
        subject 主题
        message 内容
        from_email 发件人
        recipient_list 收件人列表
        html_message html内容"""
        # 对a标签的数据进行加密处理
        # 加密user_id
        token = generic_email_verify_token(request.user.id)
        # 组织我们的激活邮件
        html_message = '点击按钮进行激活<a href="http://www.itcast.cn/?token=%s">点击激活</a>' % token
        # 返回响应
        send_mail(subject='美多商城激活邮件',message='',html_message=html_message,from_email='美多商城<qi_rui_hua@163.com>',recipient_list=['limounb123@163.com'])
        return JsonResponse({'code':0,'errmsg':'ok'})


