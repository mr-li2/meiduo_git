from apps.users.views import UsernameCountView, RegisterView, MobileCountView, LoginView, LogoutView, EmailView
from apps.users.views import CenterView
from django.urls import path


urlpatterns = [
    path('usernames/<username:username>/count/', UsernameCountView.as_view()),
    path('mobiles/<mobile:mobile>/count/',MobileCountView.as_view()),
    path('register/', RegisterView.as_view()),
    path('login/', LoginView.as_view()),
    path('logout/',LogoutView.as_view()),
    path('info/',CenterView.as_view()),
    path('emails/', EmailView.as_view())

]
