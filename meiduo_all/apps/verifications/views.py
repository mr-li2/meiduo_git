from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django_redis import get_redis_connection
# Create your views here.
from django.views import View

from libs.yuntongxun.sms import CCP

"""
前端
    拼接一个url 然后img会发送请求
    url = http://ip:port/image_codes/uuid/
后端
    请求  接收路由中的uuid
    业务逻辑 生成图片验证码和图片二进制。
    通过redis把图片验证码保存起来
    响应 返回图片二进制
    """
class ImageCodeView(View):
    def get(self,request,uuid):
        # 1.接收路由中的uuid
        # 2.生成图片验证码和图片二进制
        from libs.captcha.captcha import captcha
        # text图片内容，image是图片二进制
        text,image = captcha.generate_captcha()
        # 3.通过redis把图片验证码保存起来
        # 进行redis的连接

        redis_cli = get_redis_connection('code')
        redis_cli.setex(uuid,100,text)
        # 因为图片是二进制我们不能返回JSON数据
        # content_type = 响应体数据类型
        # content_type的语法形式是:大类/小类
        # 图片:image/jpeg,image/gif,image/png....
        return HttpResponse(image,content_type='image/jpeg')
"""
发送短信验证码
前端
    用户输入完图片验证码和手机号，前端发送一个axios请求
http://www.meiduo.site:8000/sms_codes/17716799661/?image_code=1234&image_code_id=136f85ac-d1ce-4dd7-8b21-01ef109a9e03
后端
    请求:接收请求 获取手机号 图片验证码和uuid
    业务逻辑:验证参数 
    返回响应
    """
class SmsCodeView(View):
    def get(self,request,mobile):
        # 1.获取请求参数
        image_code = request.GET.get('image_code')
        uuid = request.GET.get('image_code_id')
        # 2.验证参数
        if not all([image_code,uuid]):
            return JsonResponse({'code':400,'errmsg':'参数不全'})
        # 3.验证图片验证码
        # 3.1连接redis
        redis_cli = get_redis_connection('code')
        # 3.2获取redis数据
        redis_image_code = redis_cli.get(uuid)
        if redis_image_code is None:
            return JsonResponse({'code':400,'errmsg':'图片验证已过期'})
        if redis_image_code.decode().lower() != image_code.lower():
            return JsonResponse({'code':400,'errmsg':'图片验证错误'})
        # 提取发送短信的标记，看看有没有
        send_flag = redis_cli.get('send_flag_%s' % mobile)
        if send_flag is not None:
            return JsonResponse({'code':400,'errmsg':'不要频繁发送短信'})
        # 4.生成短信验证码
        from random import randint
        sms_code = '%06d' % randint(0,999999)
        # redis管道3步
        # 一:新建一个管道
        pipeline = redis_cli.pipeline()
        # 二:管道收集指定
        # 5.保存短信验证码
        pipeline.setex(mobile,300,sms_code)
        # 添加一个发送标记,有效期60，内容随便
        pipeline.setex('send_flag_%s'% mobile,60,1)
        # 三:管道执行指令
        pipeline.execute()
        # 6.发送短信验证码
        # CCP().send_template_sms(mobile,[sms_code,5],1)
        from celery_tasks.sms.tasks import send_sms_code
        # delay等于任务函数
        send_sms_code.delay(mobile,sms_code)
        return JsonResponse({'code':0,'errmsg':'ok'})