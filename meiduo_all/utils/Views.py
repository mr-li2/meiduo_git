from django.contrib.auth.mixins import LoginRequiredMixin, AccessMixin
from django.http import JsonResponse

# 让用户没有登录返回json数据
# 第一种方式
# class LoginRequiredJsonMixin(AccessMixin):
#     """Verify that the current user is authenticated."""
#     def dispatch(self, request, *args, **kwargs):
#         if not request.user.is_authenticated:
#             return JsonResponse({'code':400,'errmsg':'没有登录'})
#         return super().dispatch(request, *args, **kwargs)
# 第二种方式


class LoginRequiredJsonMixin(LoginRequiredMixin):
    def handle_no_permission(self):
        return JsonResponse({'code': 400, 'errmsg': '没有登录'})
