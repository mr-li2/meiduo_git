from django.urls import converters
class UsernameConverter:
    regex = '[0-9a-zA-Z]{4,20}'

    def to_python(self, value):
        return value
class PhoneConverter:
    regex = '1[3-9]\d{9}'
    def to_python(self, value):
        return value
