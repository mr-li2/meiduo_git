from libs.yuntongxun.sms import CCP
from celery_tasks.main import app
# 这个函数必须要让celery的实例的task装饰器装饰
import logging
logger = logging.getLogger('django')
# @app.task(name='send_sms_code',bind=True,default_retry_delay=10)
@app.task
def send_sms_code(mobile,sms_code):
    try:
        print(mobile,sms_code)
        result = CCP().send_template_sms(mobile, [sms_code, 5], 1)
        if result != 0:
            logger.error('发送失败,%s'%result)
            raise Exception('发送失败')
    except Exception as e:
        send_sms_code(mobile,sms_code)