import os
from celery import Celery

#1.设置django的配置文件,celery运行要使用settings文件
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "meiduo_all.settings")

#2.创建celery实例对象
app = Celery('celery_tasks')

#3.设置broker
# 通过加载配置文件来设置broker
app.config_from_object('celery_tasks.config')

#4.自动检测任务
app.autodiscover_tasks(['celery_tasks.sms'])
# 在虚拟环境下运行消费者指令
# celery -A celery_tasks.main worker -l INFO